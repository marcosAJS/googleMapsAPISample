var lastId = "";

$("[data-toggle='toggle']").click(function() {
    console.log("Change Toggle.");
    if (lastId != ""){
        var selector = lastId.data("target");
        $(selector).toggleClass('in');
    }
    if (lastId != $(this)){
        lastId = $(this);
    }

    var selector = $(this).data("target");
    $(selector).toggleClass('in');
});