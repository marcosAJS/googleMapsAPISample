var app = angular.module('app',[]);

app.controller('htmlController', function($scope, $sce){
    $scope.msg = "<h1>Text in HTML</h1>";
    $scope.trustedHtml;

    $scope.html = function(){
        $scope.trustedHtml = $sce.trustAsHtml($scope.msg);
        $scope.$apply();
    };
});