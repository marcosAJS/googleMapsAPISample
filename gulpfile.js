var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglyfly = require('gulp-uglyfly'),
    server = require('gulp-live-server');

gulp.task('sass', function () {
    return gulp.src('./assets/src/css/*.css')
        .pipe(concat('style.min.css'))
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('js', function () {
    gulp.src('./assets/src/js/**/*.js')
        // .pipe(uglyfly())
        .pipe(gulp.dest('./assets/js'));
});

gulp.task('scripts', function () {
    return gulp.src('./assets/src/js/**/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./assets/js'));
});


gulp.task('watch', function () {
    gulp.watch('./assets/src/js/**/*.js', ['js']);
    gulp.watch('./assets/src/css/*.css', ['sass']);
});


//server
gulp.task('server', function () {
    var serve = server.static('./', 8000);
    serve.start();

    //use gulp.watch to trigger server actions(notify, start or stop)
    gulp.watch(['./assets/css/**/*.css', './assets/js/**/*.js', './*.html'], function (file) {
        serve.notify.apply(serve, [file]);
    });
});

gulp.task('default', ['sass', 'js', 'watch', 'server']);